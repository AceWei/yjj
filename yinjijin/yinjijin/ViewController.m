//
//  ViewController.m
//  yinjijin
//
//  Created by 魏诗豪 on 15/6/9.
//  Copyright (c) 2015年 AceWei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self CreatewebContent];
}


//加载网页
-(void)CreatewebContent{
    NSURL* url = [NSURL URLWithString:@"http://yjb.wxpop.cn/"];//创建URL
    NSURLRequest* request = [NSURLRequest requestWithURL:url];//创建NSURLRequest
    [_WebContent loadRequest:request];//加载
    _WebContent.delegate = self;
    _WebContent.scrollView.bounces = NO;
}


-(void)webViewDidStartLoad:(UIWebView *)webView{
    self.HUD = [MBProgressHUD showHUDAddedTo:_WebContent animated:YES];
}


-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.HUD hide:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

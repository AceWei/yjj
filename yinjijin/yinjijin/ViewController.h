//
//  ViewController.h
//  yinjijin
//
//  Created by 魏诗豪 on 15/6/9.
//  Copyright (c) 2015年 AceWei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD.h>

@interface ViewController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *WebContent;

@property (weak, nonatomic) MBProgressHUD *HUD;

@end


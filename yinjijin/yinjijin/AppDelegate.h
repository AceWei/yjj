//
//  AppDelegate.h
//  yinjijin
//
//  Created by 魏诗豪 on 15/6/9.
//  Copyright (c) 2015年 AceWei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

